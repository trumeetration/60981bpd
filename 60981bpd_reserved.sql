-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 12, 2021 at 09:00 AM
-- Server version: 8.0.22-0ubuntu0.20.04.3
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `60981bpd`
--

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `id` int NOT NULL,
  `room_id` int NOT NULL,
  `guest_id` int NOT NULL,
  `reserved_at` date NOT NULL,
  `expires_at` date NOT NULL,
  `people_count` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `room_id`, `guest_id`, `reserved_at`, `expires_at`, `people_count`) VALUES
(1, 3, 1, '2021-02-10', '2021-02-12', 1),
(2, 1, 3, '2021-02-12', '2021-02-14', 2),
(3, 4, 13, '2020-12-01', '2020-12-02', 1),
(4, 5, 5, '2021-02-01', '2021-02-02', 2),
(5, 6, 12, '2021-01-20', '2021-01-27', 2),
(6, 7, 11, '2021-01-24', '2021-01-25', 1),
(7, 8, 1, '2021-02-05', '2021-02-06', 1),
(8, 9, 10, '2020-12-23', '2020-12-24', 1);

-- --------------------------------------------------------

--
-- Table structure for table `corpuses`
--

CREATE TABLE `corpuses` (
  `id` int NOT NULL COMMENT 'id корпуса',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'Наименование'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `corpuses`
--

INSERT INTO `corpuses` (`id`, `name`) VALUES
(1, 'Главный корпус'),
(2, 'Элитный корпус'),
(3, 'Бюджетный корпус'),
(4, 'СПА-корпус'),
(5, 'Капусльный корпус');

-- --------------------------------------------------------

--
-- Table structure for table `guests`
--

CREATE TABLE `guests` (
  `id` int NOT NULL,
  `full_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `guests`
--

INSERT INTO `guests` (`id`, `full_name`) VALUES
(1, 'Иванов Иван Иванович'),
(2, 'Алексеев Алексей Алексеевич'),
(3, 'Порохов Иван Михайлович'),
(4, 'Прохоров Никита Павлович'),
(5, 'Куцуев Егор Игоревич'),
(6, 'Бардаков Павел Дмитриевич'),
(7, 'Алексеев Алексей Вадимович'),
(8, 'Сиренко Николай Викторович'),
(9, 'Ахмитов Кирилл Русланович'),
(10, 'Гуртовенко Андрей Викторович'),
(11, 'Семенов Андрей Иванович'),
(12, 'Криптоев Никита Юрбевич'),
(13, 'Куцуев Ярослав Игоревич'),
(14, 'Андреев Семён Даниилович'),
(15, 'Егорова Ариана Артёмовна'),
(16, 'Высоцкая Амина Савельевна'),
(17, 'Белоусова Арина Николаевна'),
(18, 'Тихонова Екатерина Евгеньевна');

-- --------------------------------------------------------

--
-- Table structure for table `residence`
--

CREATE TABLE `residence` (
  `id` int NOT NULL,
  `room_id` int NOT NULL,
  `guest_id` int NOT NULL,
  `joined_at` date NOT NULL,
  `expires_at` date NOT NULL,
  `people_count` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `residence`
--

INSERT INTO `residence` (`id`, `room_id`, `guest_id`, `joined_at`, `expires_at`, `people_count`) VALUES
(1, 2, 7, '2021-02-01', '2021-02-02', 1),
(2, 1, 9, '2021-02-03', '2021-02-04', 2),
(3, 3, 6, '2021-02-11', '2021-02-12', 2),
(4, 1, 10, '2021-01-01', '2021-01-04', 1),
(5, 1, 8, '2021-01-20', '2021-01-21', 2),
(6, 2, 5, '2021-01-27', '2021-01-29', 2),
(7, 9, 12, '2020-12-30', '2020-12-31', 1),
(8, 8, 7, '2020-12-28', '2020-12-28', 1),
(9, 7, 5, '2020-12-27', '2020-12-28', 2),
(10, 6, 9, '2020-12-24', '2020-12-25', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int NOT NULL,
  `corpus_id` int NOT NULL COMMENT 'id корпуса',
  `number` int NOT NULL COMMENT 'номер комнаты',
  `capacity` int NOT NULL COMMENT 'количество мест',
  `price` int NOT NULL COMMENT 'цена'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `corpus_id`, `number`, `capacity`, `price`) VALUES
(1, 1, 100, 2, 4000),
(2, 2, 203, 4, 15000),
(3, 3, 205, 2, 3000),
(4, 3, 108, 5, 5000),
(5, 4, 401, 2, 5000),
(6, 5, 510, 1, 3000),
(7, 5, 520, 2, 4000),
(8, 2, 222, 1, 10000),
(9, 4, 453, 4, 12000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`),
  ADD KEY `room_id` (`room_id`),
  ADD KEY `guest_id` (`guest_id`);

--
-- Indexes for table `corpuses`
--
ALTER TABLE `corpuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `guests`
--
ALTER TABLE `guests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `residence`
--
ALTER TABLE `residence`
  ADD PRIMARY KEY (`id`),
  ADD KEY `guest_id` (`guest_id`),
  ADD KEY `room_id` (`room_id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `corpus_id` (`corpus_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `corpuses`
--
ALTER TABLE `corpuses`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'id корпуса', AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `guests`
--
ALTER TABLE `guests`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `residence`
--
ALTER TABLE `residence`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `booking`
--
ALTER TABLE `booking`
  ADD CONSTRAINT `booking_ibfk_1` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `booking_ibfk_2` FOREIGN KEY (`guest_id`) REFERENCES `guests` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `residence`
--
ALTER TABLE `residence`
  ADD CONSTRAINT `residence_ibfk_1` FOREIGN KEY (`guest_id`) REFERENCES `guests` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `residence_ibfk_2` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `rooms`
--
ALTER TABLE `rooms`
  ADD CONSTRAINT `rooms_ibfk_1` FOREIGN KEY (`corpus_id`) REFERENCES `corpuses` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
