<?php namespace App\Models;
use CodeIgniter\Model;
class RoomsModel extends Model
{
    protected $table = 'rooms'; //таблица, связанная с моделью
    public function getRooms($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }
}