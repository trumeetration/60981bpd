<?php namespace App\Models;
use CodeIgniter\Model;
class GuestsModel extends Model
{
    protected $table = 'guests'; //таблица, связанная с моделью
    protected $allowedFields = ['id','full_name', 'picture_url', 'auth_id'];
    public function getGuests($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }

    public function regUser($name, $auth_id) {
        $data = [
            'full_name' => $name,
            'auth_id' => $auth_id
        ];
        $this->insert($data);
    }

    public function getByAuthId($id) {
        return $this->where(['auth_id' => $id])->first();
}
}
