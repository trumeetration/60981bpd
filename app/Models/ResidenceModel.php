<?php namespace App\Models;
use CodeIgniter\Model;
class ResidenceModel extends Model
{
    protected $table = 'residence'; //таблица, связанная с моделью
    protected $allowedFields = ['room_id', 'guest_id', 'joined_at', 'expires_at', 'people_count'];
    public function getResidence($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }

    public function getResidenceWithUser($id = null, $search = '')
    {
        $builder = $this->select('residence.*, guests.full_name, guests.picture_url, guests.auth_id, corpuses.name, rooms.number, rooms.corpus_id')
            ->join('guests', 'residence.guest_id = guests.id')
            ->join('rooms', 'residence.room_id = rooms.id')
            ->join('corpuses', 'rooms.corpus_id = corpuses.id')
            ->like('full_name', $search,'both', null, true)
            ->orLike('name', $search,'both', null, true);
        if (isset($id))
        {
            return $builder->where(['residence.guest_id' => $id])->first();
        }
        return $builder;
    }


}