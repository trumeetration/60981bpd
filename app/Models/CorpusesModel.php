<?php namespace App\Models;
use CodeIgniter\Model;
class CorpusesModel extends Model
{
    protected $table = 'corpuses'; //таблица, связанная с моделью
    public function getCorpuses($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }
}