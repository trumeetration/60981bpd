<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Hotelus extends Migration
{
    public function up()
    {
        // activity_type
        if (!$this->db->tableexists('guests'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'full_name' => array('type' => 'VARCHAR', 'constraint' => '50', 'null' => FALSE)
            ));
            // create table
            $this->forge->createtable('guests', TRUE);
        }

        if (!$this->db->tableexists('corpuses'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'name' => array('type' => 'VARCHAR', 'constraint' => '20', 'null' => FALSE)
            ));
            // create table
            $this->forge->createtable('corpuses', TRUE);
        }

        // activity_type
        if (!$this->db->tableexists('rooms'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'corpus_id' => array('type' => 'INT', 'unsigned' => TRUE),
                'number' => array('type' => 'INT', 'null' => FALSE),
                'capacity' => array('type' => 'INT', 'null' => FALSE),
                'price' => array('type' => 'INT', 'null' => FALSE),
            ));
            $this->forge->addForeignKey('corpus_id','corpuses','id','RESTRICT','RESRICT');
            // create table
            $this->forge->createtable('rooms', TRUE);
        }

        if (!$this->db->tableexists('residence'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'room_id' => array('type' => 'INT', 'unsigned' => TRUE),
                'guest_id' => array('type' => 'INT', 'unsigned' => TRUE),
                'joined_at' => array('type' => 'DATE', 'null' => FALSE),
                'expires_at' => array('type' => 'DATE', 'null' => FALSE),
                'people_count' => array('type' => 'INT', 'null' => FALSE),
            ));
            $this->forge->addForeignKey('room_id','rooms','id','RESTRICT','RESRICT');
            $this->forge->addForeignKey('guest_id','guests','id','RESTRICT','RESRICT');
            // create table
            $this->forge->createtable('residence', TRUE);
        }

        if (!$this->db->tableexists('booking'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'room_id' => array('type' => 'INT', 'unsigned' => TRUE),
                'guest_id' => array('type' => 'INT', 'unsigned' => TRUE),
                'reserved_at' => array('type' => 'DATE', 'null' => FALSE),
                'expires_at' => array('type' => 'DATE', 'null' => FALSE),
                'people_count' => array('type' => 'INT', 'null' => FALSE),
            ));
            $this->forge->addForeignKey('room_id','rooms','id','RESTRICT','RESRICT');
            $this->forge->addForeignKey('guest_id','guests','id','RESTRICT','RESRICT');
            // create table
            $this->forge->createtable('booking', TRUE);
        }
    }

    //--------------------------------------------------------------------

    public function down()
    {
        $this->forge->droptable('corpuses');
        $this->forge->droptable('guests');
        $this->forge->droptable('rooms');
        $this->forge->droptable('residence');
        $this->forge->droptable('booking');
    }
}
