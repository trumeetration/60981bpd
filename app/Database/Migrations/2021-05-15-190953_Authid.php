<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Authid extends Migration
{
    public function up()
    {
        if ($this->db->tableexists('guests'))
        {
            $this->forge->addColumn('guests',array(
                'auth_id' => array('type' => 'INT', 'unsigned' => TRUE),
            ));
            $this->forge->addForeignKey('auth_id','users','id','RESTRICT','RESRICT');
        }
    }
    public function down()
    {
        $this->forge->dropColumn('guests', 'auth_id');
    }
}