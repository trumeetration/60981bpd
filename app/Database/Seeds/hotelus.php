<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class hotelus extends Seeder
{
    public function run()
    {
        $data = [
            'id' => 1,
            'full_name' => "Бардаков Павел Дмитриевич"
        ];
        $this->db->table('guests')->insert($data);

        $data = [
            'id' => 2,
            'full_name' => "Куцуев Егор Егорович"
        ];
        $this->db->table('guests')->insert($data);

        $data = [
            'id' => 3,
            'full_name' => "Олегов Олег Олегович"
        ];
        $this->db->table('guests')->insert($data);

        $data = [
            'id' => 4,
            'full_name' => "Бардаков Иван Михайлович"
        ];
        $this->db->table('guests')->insert($data);

        $data = [
            'id' => 5,
            'full_name' => "Ахмитов Кирилл Иванович"
        ];
        $this->db->table('guests')->insert($data);

        $data = [
            'id' => 1,
            'name' => "Главный корпус"
        ];
        $this->db->table('corpuses')->insert($data);

        $data = [
            'id' => 2,
            'name' => "Элитный корпус"
        ];
        $this->db->table('corpuses')->insert($data);

        $data = [
            'id' => 3,
            'name' => "Бюджетный корпус"
        ];
        $this->db->table('corpuses')->insert($data);

        $data = [
            'id' => 4,
            'name' => "СПА-корпус"
        ];
        $this->db->table('corpuses')->insert($data);

        $data = [
            'id' => 5,
            'name' => "Капсульный корпус"
        ];
        $this->db->table('corpuses')->insert($data);

        $data = [
            'id' => 1,
            'corpus_id' => 1,
            'number' => 100,
            'capacity' => 1,
            'price' => 1000
        ];
        $this->db->table('rooms')->insert($data);

        $data = [
            'id' => 2,
            'corpus_id' => 2,
            'number' => 200,
            'capacity' => 1,
            'price' => 1000
        ];
        $this->db->table('rooms')->insert($data);

        $data = [
            'id' => 3,
            'corpus_id' => 3,
            'number' => 300,
            'capacity' => 1,
            'price' => 1000
        ];
        $this->db->table('rooms')->insert($data);

        $data = [
            'id' => 4,
            'corpus_id' => 4,
            'number' => 400,
            'capacity' => 1,
            'price' => 1000
        ];
        $this->db->table('rooms')->insert($data);

        $data = [
            'id' => 1,
            'room_id' => 1,
            'guest_id' => 1,
            'joined_at' => '2021-01-30',
            'expires_at' => '2021-01-30',
            'people_count' => 1
        ];
        $this->db->table('residence')->insert($data);

        $data = [
            'id' => 2,
            'room_id' => 2,
            'guest_id' => 2,
            'joined_at' => '2021-01-30',
            'expires_at' => '2021-01-30',
            'people_count' => 1
        ];
        $this->db->table('residence')->insert($data);

        $data = [
            'id' => 3,
            'room_id' => 3,
            'guest_id' => 3,
            'joined_at' => '2021-01-30',
            'expires_at' => '2021-01-30',
            'people_count' => 1
        ];
        $this->db->table('residence')->insert($data);

    }
}
