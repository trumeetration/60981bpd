<?php namespace App\Services;

use Google_Client;

class GoogleClient
{
    private $google_client;
    public function __construct()
    {
        $this->google_client = new Google_Client();
        $this->google_client->setClientId('588578180670-k6e8g2fcn0rh0ole3pfrkhp0md4h7qk9.apps.googleusercontent.com');
        $this->google_client->setClientSecret('AKgQktFbWYO649FGH53Wn_qm');
        $this->google_client->setRedirectUri(base_url().'/auth/google_login');
        $this->google_client->addScope('email');
        $this->google_client->addScope('profile');
    }
    public function getGoogleClient()
    {
        return $this->google_client;
    }

}