<?php namespace App\Controllers;

use App\Models\CorpusesModel;
use App\Models\ResidenceModel;
use App\Models\GuestsModel;
use App\Models\RoomsModel;
use App\Config\AutoLoad;
use CodeIgniter\Model;
use CodeIgniter\BaseModel;
use Aws\S3\S3Client;


class Residence extends BaseController


{
    public function index() //Обображение всех записей
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new ResidenceModel();
        $data ['residence'] = $model->getResidenceWithUser()->findAll();
        $data ['auth_id'] = $this->ionAuth->user()->row()->id;
        echo view('residence/view_all', $this->withIon($data));
    }

    public function view($id = null) //отображение одной записи
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new ResidenceModel();
        $data ['residence'] = $model->getResidence($id);
        $model = new GuestsModel();
        $guest_id = $data['residence']['guest_id'];
        $data ['guest'] = $model->getGuests($guest_id);
        $data ['room'] = (new RoomsModel())->getRooms($data['residence']['room_id']);
        $data ['corpus'] = (new CorpusesModel())->getCorpuses($data['room']['corpus_id']);

        echo view('residence/view', $this->withIon($data));
    }

    public function viewAllAdmin()
    {
        if ($this->ionAuth->isAdmin())
        {
            //Подготовка значения количества элементов выводимых на одной странице
            if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
            {
                //сохранение кол-ва страниц в переменной сессии
                session()->setFlashdata('per_page', $this->request->getPost('per_page'));
                $per_page = $this->request->getPost('per_page');
            }
            else {
                $per_page = session()->getFlashdata('per_page');
                session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
                if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
            }
            $data['per_page'] = $per_page;
            //Обработка запроса на поиск
            if (!is_null($this->request->getPost('search')))
            {
                session()->setFlashdata('search', $this->request->getPost('search'));
                $search = $this->request->getPost('search');
            }
            else {
                $search = session()->getFlashdata('search');
                session()->setFlashdata('search', $search);
                if (is_null($search)) $search = '';
            }
            $data['search'] = $search;
            helper(['form','url']);
            $model = new ResidenceModel();
            $data['residence'] = $model->getResidenceWithUser(null,$search)->paginate($per_page,'group1');
            $data['pager'] = $model->pager;
            echo view('residence/view_all_admin', $this->withIon($data));
        }
        else
        {
            session()->setFlashdata('message', lang('Auth.admin_permission_needed'));
            return redirect()->to('/auth/login');
        }
    }

    public function create()
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $data ['validation'] = \Config\Services::validation();
        $data['corpuses'] = (new CorpusesModel())->getCorpuses();
        $data['rooms'] = (new RoomsModel())->getRooms();
        $data['guests'] = (new GuestsModel())->getGuests();
        echo view('residence/create', $this->withIon($data));
    }

    public function changepic()
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $data ['validation'] = \Config\Services::validation();
        $data ['auth_id'] = $this->ionAuth->user()->row()->id;
        $data ['picurl'] = (new GuestsModel())->getByAuthId($this->ionAuth->user()->row()->id)['picture_url'];
        echo view('residence/changepic', $this->withIon($data));
    }

    public function store()
    {
        helper(['form','url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
                'room_id' => 'required',
                'guest_id'  => 'required',
                'joined_at'  => 'required',
                'expires_at'  => 'required',
            ]))
        {
            $data = [
                'room_id' => $this->request->getPost('room_id'),
                'guest_id' => $this->request->getPost('guest_id'),
                'joined_at' => $this->request->getPost('joined_at'),
                'expires_at' => $this->request->getPost('expires_at'),
                'people_count' => 1,
            ];
            $model = new ResidenceModel();
            $model->save($data);

            //session()->setFlashdata('message', lang('Curating.residence_create_success'));
            return redirect()->to('/residence');
        }
        else
        {
            return redirect()->to('/residence/create')->withInput();
        }
    }

    public function updateImg()
    {
        helper(['form','url']);
        if ($this->request->getMethod() === 'post')
        {
            $insert = null;
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);

            }
            if (!is_null($insert)) {
                $model = new GuestsModel();
                $model->db->query("UPDATE guests SET picture_url = '{$insert['ObjectURL']}'  WHERE auth_id = {$this->request->getPost('auth_id')}");
            }
            //session()->setFlashdata('message', lang('Curating.rating_update_success'));

            return redirect()->to('/residence');
        }
        else
        {
            return redirect()->to('/residence/changepic')->withInput();
        }
    }

    public function update()
    {
        helper(['form','url']);
        echo '/rating/edit/'.$this->request->getPost('id');
        if ($this->request->getMethod() === 'post' && $this->validate([
                'id'  => 'required',
                'room_id' => 'required',
                'guest_id'  => 'required',
                'joined_at'  => 'required',
                'expires_at'  => 'required',
            ]))
        {
            $model = new ResidenceModel();
            $model->db->query("UPDATE residence SET room_id = {$this->request->getPost('room_id')}, guest_id = {$this->request->getPost('guest_id')}, joined_at = '{$this->request->getPost('joined_at')}', expires_at = '{$this->request->getPost('expires_at')}' WHERE id = {$this->request->getPost('id')}");
            //session()->setFlashdata('message', lang('Curating.rating_update_success'));

            return redirect()->to('/residence');
        }
        else
        {
            return redirect()->to('/residence/edit/'.$this->request->getPost('id'))->withInput();
        }
    }



    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new ResidenceModel();

        helper(['form']);
        $data ['residence'] = $model->getResidence($id);
        $data ['room_before'] = (new RoomsModel())->getRooms($data['residence']['room_id']);
        $data ['rooms'] = (new RoomsModel())->getRooms();
        $data ['corpus_before'] = (new CorpusesModel())->getCorpuses($data['room_before']['corpus_id']);
        $data ['corpuses'] = (new CorpusesModel())->getCorpuses();
        $data ['guest'] = (new GuestsModel())->getGuests($data['residence']['guest_id']);
        $data ['validation'] = \Config\Services::validation();
        echo view('residence/edit', $this->withIon($data));

    }

    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new ResidenceModel();
        $model->db->query("DELETE FROM residence WHERE id = {$id}");
        return redirect()->to('/residence');
    }


}

