<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
        <div class="container">
            <h1 class="display-3">Hotelus</h1>
            <p>Онлайн сервис гостиницы, предоставляющий множество функций и возможностей по отслеживанию состояния, занятости номеров, и еще многое другое...</p>
            <?php if (! $ionAuth->loggedIn()): ?>
                <p><a class="btn btn-primary btn-lg" href="<?=base_url()?>/auth/login" role="button">Авторизоваться</a></p>
            <?php else: ?>
                <p><a class="btn btn-primary btn-lg" href="<?=base_url()?>/residence" role="button">Осмотреться</a></p>
            <?php endif ?>

        </div>
    </div>

    <div class="container">
        <!-- Example row of columns -->
        <div class="row">
            <div class="col-md-4">
                <h2>Прайс-лист</h2>
                <p>На сайте можно узнать стоимость необходимого Вам номера!</p>
                <?php if (! $ionAuth->loggedIn()): ?>
                    <p><a class="btn btn-primary btn-lg" href="<?=base_url()?>/auth/login" role="button">Авторизоваться</a></p>
                <?php else: ?>
                    <p><a class="btn btn-primary btn-lg" href="<?=base_url()?>/residence" role="button">Осмотреться</a></p>
                <?php endif ?>
            </div>
            <div class="col-md-4">
                <h2>Отличный рейтинг</h2>
                <p>Наша гостиница проверена временем! Множество положительных отзывов можно найти в интернете!</p>
                <?php if (! $ionAuth->loggedIn()): ?>
                    <p><a class="btn btn-primary btn-lg" href="<?=base_url()?>/auth/login" role="button">Авторизоваться</a></p>
                <?php else: ?>
                    <p><a class="btn btn-primary btn-lg" href="<?=base_url()?>/residence" role="button">Осмотреться</a></p>
                <?php endif ?>
            </div>
            <div class="col-md-4">
                <h2>100% гарантия качества</h2>
                <p>Мы гарантируем отсутствие у Вас негативных эмоций и ощущений в период проживания в Нашей гостинице</p>
                <?php if (! $ionAuth->loggedIn()): ?>
                    <p><a class="btn btn-primary btn-lg" href="<?=base_url()?>/auth/login" role="button">Авторизоваться</a></p>
                <?php else: ?>
                    <p><a class="btn btn-primary btn-lg" href="<?=base_url()?>/residence" role="button">Осмотреться</a></p>
                <?php endif ?>
            </div>
        </div>

        <hr>

    </div> <!-- /container -->
<?= $this->endSection() ?>
