<!DOCTYPE html>
<head>
    <title>Hotelus</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/6e9b058a28.js"></script>
    <meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <style>
        main{
            margin-top: 90px;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="<?=base_url()?>">Hotelus</a>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="<?=base_url()?>/residence">Проживания</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url()?>/residence/store">Добавить проживание</a>
            </li>
        </ul>
        <?php if (! $ionAuth->loggedIn()): ?>
            <a class="btn btn-outline-success" href="<?=base_url()?>/auth/login">Войти</a>
        <?php else: ?>
            <div class="btn-group">
                <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Профиль
                </button>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" type="button" href="<?=base_url()?>/residence/changepic">Сменить фото</a>
                    <a class="dropdown-item" type="button" href="<?=base_url()?>/auth/logout">Выйти</a>
                </div>
            </div>
        <?php endif ?>

    </div>
</nav>
<?= $this->renderSection('content') ?>
<footer class="text-center">
    <p>© Павел Бардаков 2021</p>
</footer>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>
</html>