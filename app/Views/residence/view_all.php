<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container mt-4">
            <?php use CodeIgniter\I18n\Time; ?>
            <h2 class="mb-4 ml-3">Список проживающих гостей</h2>
            <?php if (!empty($residence) && is_array($residence)) : ?>
                <div class="row hidden-md-up">
                    <?php foreach ($residence as $item): ?>
                        <div class="col-md-3 mb-3">
                            <div class="card">
                            <?php if (!is_null($item['picture_url'])) : ?>
                                <img src="<?= $item['picture_url'] ?>" class="card-img-top p-3" height="250" alt="oops" />
                            <?php else:?>
                                <img src="/user.svg" class="card-img-top p-3" height="250" alt="oops" />
                            <?php endif ?>
                                    <div class="card-body">
                                    <h5 class="card-title">ФИО: <?= $item['full_name'] ?> </h5>
                                    <p class="card-text">Дата заселения: <?= esc(Time::parse($item['joined_at'])->toDateString() ); ?></p>
                                    <a href="<?= base_url()?>/residence/view/<?= esc($item['id']); ?>" class="btn btn-outline-info">Просмотреть</a>
                                    <?php if ($item['auth_id'] == $auth_id) : ?>
            <a href="<?= base_url()?>/residence/edit/<?= esc($item['id']); ?>" class="btn btn-outline-warning mt-2">Редактировать</a>
                                    <?php endif ?>
                                    </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php else : ?>
                <p>Невозможно найти записи</p>
            <?php endif ?>
    </div>

<?= $this->endSection() ?>