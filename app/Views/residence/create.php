<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container" style="max-width: 540px;">

        <?= form_open_multipart('residence/store'); ?>
        <div class="input-group my-3">
            <div class="input-group-prepend">
                <label class="input-group-text" for="room_id">Комната</label>
            </div>
            <select class="custom-select" id="room_id" name="room_id">
                <option selected>Выбрать...</option>
                <?php foreach ($rooms as $room): ?>
                    <option value="<?= $room['id'] ?>"><?= $room['number'] ?> - <?= $corpuses[$room['corpus_id']-1]['name'] ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="input-group my-3">
            <div class="input-group-prepend">
                <label class="input-group-text" for="guest_id">Гость</label>
            </div>
            <select class="custom-select" id="guest_id" name="guest_id">
                <option selected>Выбрать...</option>
                <?php foreach ($guests as $guest): ?>
                    <option value="<?= $guest['id'] ?>"><?= $guest['full_name'] ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="form-group">
            <label for="joined_at">С</label>
            <input type="date" class="form-control <?= ($validation->hasError('joined_at')) ? 'is-invalid' : ''; ?>" name="joined_at" value="<?= old('joined_at'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('joined_at') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="expires_at">До</label>
            <input type="date" class="form-control <?= ($validation->hasError('expires_at')) ? 'is-invalid' : ''; ?>" name="expires_at" value="<?= old('expires_at'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('expires_at') ?>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-success" name="submit">Добавить</button>
        </div>
        </form>


    </div>
<?= $this->endSection() ?>