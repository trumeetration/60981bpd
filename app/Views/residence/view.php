<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main mt-4">
    <?php use CodeIgniter\I18n\Time; ?>
    <?php if (!empty($residence) && !empty($guest)) : ?>
        <div class="card-deck">
            <div class="card mb-3" style="max-width: 540px;">
                <div class="row align-items-center mt-4">
                    <div class="col-md-4 d-flex">
                        <?php if (!is_null($guest['picture_url'])) : ?>
                            <img src="<?= $guest['picture_url'] ?>" class="card-img-top p-3" alt="oops" />
                        <?php else:?>
                            <img src="/user.svg" class="card-img-top p-3" alt="oops" />
                        <?php endif ?>
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title"><?= $guest['full_name'] ?></h5>

                            <div class="d-flex justify-content-between">
                                <div class="my-0">Дата заселения: </div>
                                <div class="text-muted"><?= esc(Time::parse($residence['joined_at'])->toDateString() ); ?></div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Оплачено до: </div>
                                <div class="text-muted"><?= esc(Time::parse($residence['expires_at'])->toDateString() ); ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mb-3" style="max-width: 540px;">
                <div class="row no-gutters align-items-center">
                    <div class="col-4">
                        <img src="\hotel.svg" alt="...">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title"><?= $corpus['name'] ?></h5>
                            <p class="card-text">Номер комнаты: <?= $room['number'] ?></p>
                            <p class="card-text">Вместимость: <?= $room['capacity'] ?> человек(-а)</p>
                            <p class="card-text">В номере проживает: <?= esc($residence['people_count']); ?> человек(-а)</p>
                            <p class="card-text">Стоимость: <?= $room['price'] ?> Руб</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php else : ?>
        <p>Данные не найдены.</p>
    <?php endif ?>
</div>
<?= $this->endSection() ?>