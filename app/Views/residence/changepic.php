<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container" style="max-width: 540px;">
<?php if (!is_null($picurl)) : ?>
    <img src="<?= $picurl ?>" class="rounded mx-auto d-block" height="250" alt="oops" />
<?php endif ?>

        <?= form_open_multipart('residence/updateImg'); ?>

        <input type="hidden" name="auth_id" value="<?= $auth_id ?>">
        <div class="form-group">
            <label for="picture">Изображение</label>
            <input type="file" class="form-control-file <?= ($validation->hasError('picture')) ? 'is-invalid' : ''; ?>" name="picture">
            <div class="invalid-feedback">
                <?= $validation->getError('picture') ?>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-success" name="submit">Сохранить</button>
        </div>
        </form>


    </div>
<?= $this->endSection() ?>