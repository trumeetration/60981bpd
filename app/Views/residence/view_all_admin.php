<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container mt-4">
        <?php if (!empty($residence) && is_array($residence)) : ?>
            <h2 class="mb-4">Список проживающих гостей</h2>
            <div class="d-flex justify-content-start mb-2">
                <?= $pager->links('group1','my_page') ?>
                <?= form_open('residence/viewAllAdmin', ['style' => 'display: flex']); ?>
                <select name="per_page" class="ml-3" aria-label="per_page">
                    <option value="2" <?php if($per_page == '2') echo("selected"); ?>>2</option>
                    <option value="5"  <?php if($per_page == '5') echo("selected"); ?>>5</option>
                    <option value="10" <?php if($per_page == '10') echo("selected"); ?>>10</option>
                    <option value="20" <?php if($per_page == '20') echo("selected"); ?>>20</option>
                </select>
                <button class="btn btn-outline-success" type="submit" class="btn btn-primary">На странице</button>
                </form>
                <?= form_open('residence/viewAllAdmin',['style' => 'display: flex']); ?>
                <input type="text" class="form-control ml-3" name="search" placeholder="ФИО или корпус" aria-label="Search"
                       value="<?= $search; ?>">
                <button class="btn btn-outline-success" type="submit" class="btn btn-primary">Найти</button>
                </form>
            </div>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th scope="col">ФИО</th>
                    <th scope="col">Номер</th>
                    <th scope="col">Корпус</th>
                    <th scope="col">Оплачено до</th>
                    <th scope="col">Действия</th>
                </tr>
                </thead>
                <tbody>
                <?php  foreach ($residence as $data): ?>
                    <tr>
                        <td><?= $data['full_name'] ?></td>
                        <td><?= $data['number'] ?></td>
                        <td><?= $data['name'] ?></td>
                        <td><?= $data['expires_at'] ?></td>
                        <td>
                            <a href="<?= base_url()?>/residence/edit/<?= esc($data['id']); ?>" class="btn btn-warning btn-sm">Редактировать</a>
                            <a href="<?= base_url()?>/residence/delete/<?= esc($data['id']); ?>" class="btn btn-danger btn-sm">Удалить</a>
                        </td>

                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php else : ?>
            <div class="text-center">
                <p>Ничего не найдено</p>
                <a class="btn btn-success" href="<?= base_url()?>/residence/create">Создать</a>
            </div>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>